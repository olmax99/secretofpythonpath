import sys
import os


from environvars.fetch_envs import FetchEnvs


if __name__ == '__main__':

    print(f'''
-----------------------------------------------------------------------------
                        The Secret of PYTHONPATH
-----------------------------------------------------------------------------
PYTHONPATH in terminal does not exist.
In sys.path ./src is added, whereas in pyCharm PYTHONPATH has no ./src added.
Once 'run.py' is started the following way, ./src is basically removed from sys.path: 
\n\tPYTHONPATH=$(pwd) python -m pipenv run python src/run.py\n
..now, both pyCharm and terminal behave the same way. In addition \
all packages are \nloaded from the pipenv environment.

PYTHONPATH in PyCharm: \n {os.getcwd()} \n
PYTHONPATH in Terminal: \n {os.environ['PYTHONPATH']} \n
sys.path: \n{sys.path[0]}
''')

    # Simulate capturing environment variables
    print("\nEnvironment Vars:")
    ci_vars_df = FetchEnvs.env_vars()
    print(ci_vars_df)
