import os
import pandas as pd


class FetchEnvs:

    @staticmethod
    def env_vars():
        env_df = pd.DataFrame([{'Env': k, 'Val': os.environ[k]} for k in os.environ])
        env_df = env_df.loc[env_df['Env'].apply(lambda s: "SHELL" in s)]

        return env_df
