import os

import pandas as pd

from environvars.fetch_envs import FetchEnvs


def test_env_vars(monkeypatch):
    # simulate environment variables as in os.environvars
    d = {'SHELL_SESSION_ID': '2cc416a3306f4d6bb7baa1d08782ef2c',
         'SHELL': '/bin/bash',
         'PYENV_SHELL': 'bash',
         'TZ': 'Europe/Berlin',
         'XDG_CURRENT_DESKTOP': 'KDE'}
    expected_df = pd.DataFrame({'Env': ['SHELL_SESSION_ID', 'SHELL', 'PYENV_SHELL'],
                                'Val': ['2cc416a3306f4d6bb7baa1d08782ef2c', '/bin/bash', 'bash']})
    monkeypatch.setattr(os, 'environ', d)
    pd.testing.assert_frame_equal(FetchEnvs.env_vars(), expected_df)

